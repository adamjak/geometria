package net.adamjak.thomas.geometria.classes;

public class Trojuholnik
{
	private Bod bodA;
	private Bod bodB;
	private Bod bodC;
	
	private Double stranaA;
	private Double stranaB;
	private Double stranaC;
	
	private Priamka priamkaA;
	private Priamka priamkaB;
	private Priamka priamkaC;
	
	private Double va;
	private Double vb;
	private Double vc;
	
	private Double ta;
	private Double tb;
	private Double tc;
	
	private Uhol uholAlfa;
	private Uhol uholBeta;
	private Uhol uholGama;
	
	private Double r;
	private Double ro;
	
	private Double P;
	
	private Double o;
	
	public void setSSS(Double a, Double b, Double c)
	{
		this.stranaA = a;
		this.stranaB = b;
		this.stranaC = c;
		
		this.uholAlfa = new Uhol(Math.acos((b*b + c*c - a*a) / (2 * b * c)));
		this.uholBeta = new Uhol(Math.acos((a*a + c*c - b*b) / (2 * a * c)));
		this.uholGama = new Uhol(Math.acos((b*b + a*a - c*c) / (2 * b * a)));
		
		Double deltaX = b * Math.cos(2*Math.PI - this.uholAlfa.getUhol());
		Double deltaY = b * Math.sin(2*Math.PI - this.uholAlfa.getUhol());
		
		this.bodA = new Bod(0.0,0.0);
		this.bodB = new Bod(c + 0, 0.0);
		this.bodC = new Bod(deltaX, deltaY);
		
		this.priamkaA = new Priamka(this.bodB, this.bodC);
		this.priamkaB = new Priamka(this.bodA, this.bodC);
		this.priamkaC = new Priamka(this.bodB, this.bodA);
		
		this.o = this.stranaA + this.stranaB + this.stranaC;
		
		this.P = Math.sqrt((this.o / 2) * ((this.o / 2) - this.stranaA) * ((this.o / 2) - this.stranaB) * ((this.o / 2) - this.stranaC)); 
		
		this.va = (2 / this.stranaA) * this.P;
		this.vb = (2 / this.stranaB) * this.P;
		this.vc = (2 / this.stranaC) * this.P;
		
		this.ta = 1.0/2.0 * Math.sqrt(2 * (b*b + c*c) - a*a);
		this.tb = 1.0/2.0 * Math.sqrt(2 * (a*a + c*c) - b*b);
		this.tc = 1.0/2.0 * Math.sqrt(2 * (b*b + a*a) - c*c);
		
		this.r = a / (2 * Math.sin(this.uholAlfa.getUhol()));
		this.ro = Math.sqrt((((this.o / 2) - this.stranaA) * ((this.o / 2) - this.stranaB) * ((this.o / 2) - this.stranaC))/(this.o / 2));
	}
	
	public void setSUS(Double b, Double c, Uhol a)
	{
		Double stranaA = Math.sqrt( b*b + c*c - 2 * b * c * Math.cos(a.getUhol()) );
		this.setSSS(stranaA, b, c);
	}
	
	public void setUSU(Double s, Uhol a, Uhol b)
	{
		Double stranaB = (s * Math.sin(a.getUhol())) / (Math.sin(a.getUhol() + b.getUhol()));
		this.setSUS(s, stranaB, b);
	}
	
	public Bod getBodA()
	{
		return bodA;
	}
	public Bod getBodB()
	{
		return bodB;
	}
	public Bod getBodC()
	{
		return bodC;
	}
	public Double getStranaA()
	{
		return stranaA;
	}
	public Double getStranaB()
	{
		return stranaB;
	}
	public Double getStranaC()
	{
		return stranaC;
	}
	public Double getVa()
	{
		return va;
	}
	public Double getVb()
	{
		return vb;
	}
	public Double getVc()
	{
		return vc;
	}
	public Double getTa()
	{
		return ta;
	}
	public Double getTb()
	{
		return tb;
	}
	public Double getTc()
	{
		return tc;
	}
	public Uhol getUholAlfa()
	{
		return uholAlfa;
	}
	public Uhol getUholBeta()
	{
		return uholBeta;
	}
	public Uhol getUholGama()
	{
		return uholGama;
	}
	public Double getR()
	{
		return r;
	}
	public Double getRo()
	{
		return ro;
	}
	public Double getP()
	{
		return P;
	}
	public Double getO()
	{
		return o;
	}

	public Priamka getPriamkaA()
	{
		return priamkaA;
	}

	public void setPriamkaA(Priamka priamkaA)
	{
		this.priamkaA = priamkaA;
	}

	public Priamka getPriamkaB()
	{
		return priamkaB;
	}

	public void setPriamkaB(Priamka priamkaB)
	{
		this.priamkaB = priamkaB;
	}

	public Priamka getPriamkaC()
	{
		return priamkaC;
	}

	public void setPriamkaC(Priamka priamkaC)
	{
		this.priamkaC = priamkaC;
	}
	
}
