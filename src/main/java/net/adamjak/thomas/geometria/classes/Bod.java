package net.adamjak.thomas.geometria.classes;

public class Bod
{
	private final Double ODCHYLKA = 0.0001D;
	
	private Double x;
	private Double y;
	
	public Bod (Double x, Double y)
	{
		if (x == null)
		{
			throw new NullPointerException("X nesmie byt null.");
		}
		
		if (y == null)
		{
			throw new NullPointerException("Y nesmie byt null.");
		}
		
		this.x = x;
		this.y = y;
	}
	
	public Double getX()
	{
		return x;
	}
	
	public Double getY()
	{
		return y;
	}
	
	public Double getVzdialenost(Bod b)
	{
		if (b == null)
		{
			throw new NullPointerException();
		}
		
		if (b.equals(this))
		{
			return 0.0;
		}
		
		return Math.sqrt((this.x - b.x) * (this.x - b.x) + (this.y - b.y) * (this.y - b.y));
	}
	
	@Override
	public String toString()
	{
		return "Bod [x=" + x + ", y=" + y + "]";
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((x == null) ? 0 : x.hashCode());
		result = prime * result + ((y == null) ? 0 : y.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		
		if (obj == null)
		{
			return false;
		}
		
		if (getClass() != obj.getClass())
		{
			return false;
		}
		
		Bod other = (Bod) obj;
		
		if (Math.abs(this.x - other.x) <= this.ODCHYLKA && Math.abs(this.y - other.y) <= this.ODCHYLKA)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
}
