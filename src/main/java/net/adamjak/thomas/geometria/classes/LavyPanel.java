package net.adamjak.thomas.geometria.classes;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class LavyPanel extends JPanel
{
	private static final long serialVersionUID = 1L;
	
	private Trojuholnik trojuholnik;
	
	public LavyPanel()
	{
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		this.setAlignmentX(Component.LEFT_ALIGNMENT);
		this.setPreferredSize(new Dimension(250, 0));
	}
	
	public void vypisTrojuholnik(Trojuholnik t)
	{
		this.trojuholnik = t;
		
		this.removeAll();
		
		this.add(new JLabel("Trojuholník ABC"));
		
		this.add(Box.createRigidArea(new Dimension(10, 5)));
		
		this.add(new JLabel("|AB| = c = " + this.trojuholnik.getStranaC()));
		this.add(new JLabel("|BC| = a = " + this.trojuholnik.getStranaA()));
		this.add(new JLabel("|AC| = b = " + this.trojuholnik.getStranaB()));
		
		this.add(Box.createRigidArea(new Dimension(10, 5)));
		
		this.add(new JLabel("|∠CAB| = α = " + this.trojuholnik.getUholAlfa().getStupne() + "° " + this.trojuholnik.getUholAlfa().getMinuty() + "' " + String.format("%.2f", this.trojuholnik.getUholAlfa().getSekundy()) + "''"));
		this.add(new JLabel("|∠ABC| = β = " + this.trojuholnik.getUholBeta().getStupne() + "° " + this.trojuholnik.getUholBeta().getMinuty() + "' " + String.format("%.2f", this.trojuholnik.getUholBeta().getSekundy()) + "''"));
		this.add(new JLabel("|∠BCA| = γ = " + this.trojuholnik.getUholGama().getStupne() + "° " + this.trojuholnik.getUholGama().getMinuty() + "' " + String.format("%.2f", this.trojuholnik.getUholGama().getSekundy()) + "''"));
		
		this.add(Box.createRigidArea(new Dimension(10, 5)));
		
		this.add(new JLabel("<html>|v<sub>a</sub>| = " + String.format("%.2f",this.trojuholnik.getVa()) + "</html>"));
		this.add(new JLabel("<html>|v<sub>b</sub>| = " + String.format("%.2f",this.trojuholnik.getVb()) + "</html>"));
		this.add(new JLabel("<html>|v<sub>c</sub>| = " + String.format("%.2f",this.trojuholnik.getVc()) + "</html>"));
		
		this.add(Box.createRigidArea(new Dimension(10, 5)));
		
		this.add(new JLabel("<html>|t<sub>a</sub>| = " + String.format("%.2f",this.trojuholnik.getTa()) + "</html>"));
		this.add(new JLabel("<html>|t<sub>b</sub>| = " + String.format("%.2f",this.trojuholnik.getTb()) + "</html>"));
		this.add(new JLabel("<html>|t<sub>c</sub>| = " + String.format("%.2f",this.trojuholnik.getTc()) + "</html>"));
		
		this.add(Box.createRigidArea(new Dimension(10, 5)));
		
		this.add(new JLabel("|OA| = |OB| = |OC| = r = " + String.format("%.2f",this.trojuholnik.getR())));
		this.add(new JLabel("<html>|SG<sub>a</sub>| = |SG<sub>b</sub>| = |SG<sub>c</sub>| = ϱ = " + String.format("%.2f",this.trojuholnik.getRo()) + "</html>"));
		
		this.add(Box.createRigidArea(new Dimension(10, 5)));
		
		this.add(new JLabel("o = " + String.format("%.2f",this.trojuholnik.getO())));
		this.add(new JLabel("P = " + String.format("%.2f",this.trojuholnik.getP())));
		
		revalidate();
		repaint();
	}
	
}
