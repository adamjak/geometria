package net.adamjak.thomas.geometria.classes;

import java.util.ArrayList;
import java.util.List;

public class Priamka
{
	private Double a, b, c, k, q;
	
	public Priamka (Double k, Double q)
	{
		if (k == null)
		{
			throw new NullPointerException("K nesmie byt prazdne.");
		}
		
		if (q == null)
		{
			throw new NullPointerException("Q nesmie byt prazdne.");
		}
		
		this.k = k;
		this.q = q;
		
		this.a = this.k;
		this.b = -1.0;
		this.c = this.q;
	}
	
	public Priamka (Double a, Double b, Double c)
	{
		if (a == null)
		{
			throw new NullPointerException("A nesmie byt prazdne.");
		}
		
		if (b == null)
		{
			throw new NullPointerException("B nesmie byt prazdne.");
		}
		
		if (c == null)
		{
			throw new NullPointerException("C nesmie byt prazdne.");
		}
		
		if (b == 0)
		{
			this.a = 1.0;
			this.b = b;
			this.c = c / a;
			
			this.k = Double.NaN;
			this.q = Double.NaN;
		}
		else
		{
			this.a = a;
			this.b = b;
			this.c = c;
			
			this.k = - (a / b);
			this.q = - (c / b);
		}
	}
	
	public Priamka(Bod b1, Bod b2)
	{
		if (b1 == null)
		{
			throw new NullPointerException("Bod b1 nesmie byt null.");
		}
		
		if (b2 == null)
		{
			throw new NullPointerException("Bod b2 nesmie byt null.");
		}
		
		if (b1.equals(b2))
		{
			throw new IllegalArgumentException("Body su zhodne");
		}
		
		if (!b1.getX().equals(b2.getX()))
		{
			this.k = (b1.getY() - b2.getY()) / (b1.getX() - b2.getX());
			this.q = - this.k * b1.getX() + b1.getY();
			
			this.a = this.k;
			this.b = -1.0;
			this.c = this.q;
		}
		else
		{
			this.k = Double.NaN;
			this.q = Double.NaN;
			
			this.a = 1.0;
			this.b = 0.0;
			this.c = - b1.getX();
		}
		
	}
	
	public Boolean isBodOnPriamka(Bod b)
	{
		if ((this.a * b.getX() + this.b * b.getY() + this.c) == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public Bod priesecnikPriamok(Priamka p)
	{
		if (p == null)
		{
			throw new NullPointerException("Zadana priamka nesmie byt null.");
		}
		
		if (p.isPriamkaRovnobezna(this))
		{
			throw new IllegalArgumentException("Priemky nemaju priesecnik pretoze su rovnobezne.");
		}
		
		Double x, y;
		
		if (!this.k.isNaN())
		{
			if (!p.k.isNaN())
			{
				x = (- this.q + p.getQ()) / (this.k - p.getK());
				y = this.k * x + this.q;
			}
			else
			{
				x = - p.getC();
				y = this.k * x + this.q;
			}
			
		}
		else
		{
			x = -this.c;
			y = p.getK() * x + p.getQ();
		}
		
		return new Bod(x, y);
	}
	
	public Boolean isPriamkaRovnobezna(Priamka p)
	{
		if (p == null)
		{
			throw new NullPointerException("Zadana priamka nesmie byt null.");
		}
		
		if (p.getK() == this.k)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public List<Bod> getBodyMimoPlatna(Integer maxWidth, Integer maxHeight)
	{
		List<Bod> body = new ArrayList<Bod>();
		
		Double minX = 0.0;
		Double minY = 0.0;
		Double maxX = new Double(maxWidth.doubleValue());
		Double maxY = new Double(maxHeight.doubleValue());
		
		Double priesecnikSMinY = this.getYByX(minX);
		Double priesecnikSMinX = this.getXByY(minY);
		Double priesecnikSMaxY = this.getYByX(maxX);
		Double priesecnikSMaxX = this.getXByY(maxY);
		
		if (priesecnikSMinY.doubleValue() >= minY.doubleValue() && priesecnikSMinY.doubleValue() <= maxY.doubleValue() && !priesecnikSMinY.isNaN())
		{
			Bod b = new Bod(minX, priesecnikSMinY);
			
			if (!body.contains(b))
			{
				body.add(b);
			}
		}
		
		if (priesecnikSMinX.doubleValue() >= minX.doubleValue() && priesecnikSMinX.doubleValue() <= maxX.doubleValue() && !priesecnikSMinX.isNaN())
		{
			Bod b = new Bod(priesecnikSMinX, minY);
			
			if (!body.contains(b))
			{
				body.add(b);
			}
		}
		
		if (priesecnikSMaxY.doubleValue() >= minY.doubleValue() && priesecnikSMaxY.doubleValue() <= maxY.doubleValue() && !priesecnikSMaxY.isNaN())
		{
			Bod b = new Bod(maxX, priesecnikSMaxY);
			
			if (!body.contains(b))
			{
				body.add(b);
			}
		}
		
		if (priesecnikSMaxX.doubleValue() >= minX.doubleValue() && priesecnikSMaxX.doubleValue() <= maxX.doubleValue() && !priesecnikSMaxX.isNaN())
		{
			Bod b = new Bod(priesecnikSMaxX, maxY);
			
			if (!body.contains(b))
			{
				body.add(b);
			}
		}
		
		if (body.size() == 2)
		{
			return body;
		}
		else
		{
			throw new RuntimeException("Zistenych bodov bolo " + body.size() + " a musia byt prave 2.");
		}
	}
	
	public Priamka getKolmica(Bod b)
	{
		if (b == null)
		{
			throw new NullPointerException("Musi byt zadany bod v ktorom bude kolmica vedena");
		}
		
		if (this.k.doubleValue() == 0.0)
		{
			return new Priamka(1.0, 0.0, (b.getX() * (-1.0)));
		}
		else if (this.k.isNaN())
		{
			return new Priamka(0.0, b.getY());
		}
		else
		{
			return new Priamka(((-1)/this.k), (b.getY() - (((-1)/this.k) * b.getX())));
		}
	}
	
	public List<Priamka> getOsiUhla (Priamka p)
	{
		if (p == null) throw new NullPointerException("Je treba zadat priemku");
		if (this.isPriamkaRovnobezna(p)) throw new RuntimeException("Priemky su rovnobezne.");
		
		Double smernica1, smernica2, smernica3;
		
		if (this.k.isNaN())
		{
			smernica1 = Math.PI / 2;
		}
		else if (this.k.doubleValue() >= 0.0)
		{
			smernica1 = Math.atan(this.k.doubleValue());
		}
		else
		{
			smernica1 = Math.PI - Math.atan(Math.abs(this.k.doubleValue()));
		}
		
		if (p.getK().isNaN())
		{
			smernica2 = Math.PI / 2;
		}
		else if (p.getK().doubleValue() >= 0.0)
		{
			smernica2 = Math.atan(p.getK().doubleValue());
		}
		else
		{
			smernica2 = Math.PI - Math.atan(Math.abs(p.getK().doubleValue()));
		}
		
		Double rozdielUhlov = Math.abs(smernica1 - smernica2);
		
		smernica3 = Math.min(smernica1, smernica2) + rozdielUhlov / 2;
		
		Bod priesecnik = this.priesecnikPriamok(p);
		
		List<Priamka> osi = new ArrayList<Priamka>();
		
		if (smernica3 == (Math.PI / 2) || smernica3 == (-(Math.PI / 2)))
		{
			osi.add(new Priamka(1.0, 0.0,priesecnik.getX() * (-1.0)));
		}
		else
		{
			osi.add(new Priamka(Math.tan(smernica3), (priesecnik.getY() - Math.tan(smernica3) * priesecnik.getX())));
		}
		
		osi.add(osi.get(0).getKolmica(priesecnik));
		
		return osi;
	}
	
	@Override
	public String toString()
	{
		return "Priamka [a=" + a + ", b=" + b + ", c=" + c + ", k=" + k + ", q=" + q + "]";
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((a == null) ? 0 : a.hashCode());
		result = prime * result + ((b == null) ? 0 : b.hashCode());
		result = prime * result + ((c == null) ? 0 : c.hashCode());
		result = prime * result + ((k == null) ? 0 : k.hashCode());
		result = prime * result + ((q == null) ? 0 : q.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		
		if (obj == null)
		{
			return false;
		}
		
		if (getClass() != obj.getClass())
		{
			return false;
		}
		
		Priamka other = (Priamka) obj;
		
		if (a == null)
		{
			if (other.a != null)
			{
				return false;
			}
		}
		else if (!a.equals(other.a))
		{
			return false;
		}
		
		if (b == null)
		{
			if (other.b != null)
				return false;
		}
		else if (!b.equals(other.b))
		{
			return false;
		}
		
		if (c == null)
		{
			if (other.c != null)
			{
				return false;
			}
		}
		else if (!c.equals(other.c))
		{
			return false;
		}
		
		return true;
	}
	
	public Double getXByY(Double y)
	{
		Double x;
		
		if (this.k.isNaN())
		{
			x = -c;
		}
		else if (this.k == 0.0)
		{
			x = Double.NaN;
		}
		else
		{
			x = (y - this.q) / this.k;
		}
		
		return x;
	}
	
	public Double getYByX(Double x)
	{
		Double y;
		
		if (this.k.isNaN())
		{
			y = Double.NaN;
		}
		else
		{
			y = this.k * x + this.q;
		}
		
		return y;
	}
	
	public Double getA()
	{
		return a;
	}

	public void setA(Double a)
	{
		this.a = a;
	}

	public Double getB()
	{
		return b;
	}

	public void setB(Double b)
	{
		this.b = b;
	}

	public Double getC()
	{
		return c;
	}

	public void setC(Double c)
	{
		this.c = c;
	}

	public Double getK()
	{
		return k;
	}

	public void setK(Double k)
	{
		this.k = k;
	}

	public Double getQ()
	{
		return q;
	}

	public void setQ(Double q)
	{
		this.q = q;
	}
	
}
