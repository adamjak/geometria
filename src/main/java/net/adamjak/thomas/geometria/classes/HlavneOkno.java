package net.adamjak.thomas.geometria.classes;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class HlavneOkno extends JFrame
{
	private static final long serialVersionUID = 1L;
	public Container obsah;
	public Platno platno = new Platno();
	public LavyPanel lavyPanel = new LavyPanel();
	public Trojuholnik trojuholnik = null;
	
	public HlavneOkno()
	{
		this.setTitle("Geometria");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(new Dimension(900,700));
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		
		obsah = this.getContentPane();
		obsah.setLayout(new BorderLayout());
		
		obsah.add(platno, BorderLayout.CENTER);
		obsah.add(lavyPanel, BorderLayout.LINE_START);
		
		JMenuBar jmbHorneMenu = new JMenuBar();
		
		JMenu jmGeomTvary = new JMenu("Geometrické tvary");
		
		JMenuItem jmiTrojuholnik = new JMenuItem("Trojuholnik");
		jmiTrojuholnik.addActionListener(new GeomTvarTrojuholnik());
		jmGeomTvary.add(jmiTrojuholnik);
		
		jmbHorneMenu.add(jmGeomTvary);
		this.setJMenuBar(jmbHorneMenu);
	}
	
	class GeomTvarTrojuholnik implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			String[] metodyVstupu = {"SSS","SUS","SSU","USU"};
			JComboBox<String> jcbMetodaVstupu = new JComboBox<String>(metodyVstupu);
			
			Integer vstup = JOptionPane.showConfirmDialog(null, jcbMetodaVstupu, "Vyberte metódu vstupu", JOptionPane.OK_CANCEL_OPTION);
			
			if (vstup == JOptionPane.OK_OPTION)
			{
				switch (jcbMetodaVstupu.getSelectedIndex())
				{
					case 0:
							JTextField jtfStranaA = new JTextField();
							JTextField jtfStranaB = new JTextField();
							JTextField jtfStranaC = new JTextField();
							Object[] polozky = {
									"Strana a: ", jtfStranaA,
									"Strana b: ", jtfStranaB,
									"Strana c: ", jtfStranaC
							};
							
							while (jtfStranaA.getText().equals("") || jtfStranaB.getText().equals("") || jtfStranaC.getText().equals(""))
							{
								Integer sss = JOptionPane.showConfirmDialog(null, polozky, "Zadajte dĺžky strán", JOptionPane.OK_CANCEL_OPTION);
							
								if (sss == JOptionPane.OK_OPTION)
								{
									if (jtfStranaA.getText().equals("") || jtfStranaB.getText().equals("") || jtfStranaC.getText().equals(""))
									{
										JOptionPane.showMessageDialog(null, "Je potrebné zadať všetky strany.", "Chyba", JOptionPane.ERROR_MESSAGE);
									}
									else
									{
										trojuholnik = new Trojuholnik();
										trojuholnik.setSSS(Double.valueOf(jtfStranaA.getText()), Double.valueOf(jtfStranaB.getText()), Double.valueOf(jtfStranaC.getText()));
										platno.kresliTrojuholnik(trojuholnik);
										lavyPanel.vypisTrojuholnik(trojuholnik);
										break;
									}
								}
								else
								{
									break;
								}
							}
						break;
					case 1:
							JTextField jtfStranaA1 = new JTextField();
							JTextField jtfStranaB1 = new JTextField();
							JTextField jtfUholStupne = new JTextField();
							JTextField jtfUholMinuty = new JTextField();
							JTextField jtfUholSekundy = new JTextField();
							
							Object[] polozky1 = {
									"Strana a: ", jtfStranaA1,
									"Strana b: ", jtfStranaB1,
									"Uhol medzi stranami: ", jtfUholStupne, jtfUholMinuty, jtfUholSekundy
							};
							
							while (jtfStranaA1.getText().equals("") || jtfStranaB1.getText().equals("") || jtfUholStupne.getText().equals(""))
							{
								Integer sus = JOptionPane.showConfirmDialog(null, polozky1, "Zadajte dĺžky 2 strán a uhol ktorý zvierajú", JOptionPane.OK_CANCEL_OPTION);
							
								if (sus == JOptionPane.OK_OPTION)
								{
									if (jtfStranaA1.getText().equals("") || jtfStranaB1.getText().equals("") || jtfUholStupne.getText().equals(""))
									{
										JOptionPane.showMessageDialog(null, "Je potrebné zadať všetky strany a uhol.", "Chyba", JOptionPane.ERROR_MESSAGE);
									}
									else
									{
										trojuholnik = new Trojuholnik();
										trojuholnik.setSUS(
												Double.valueOf(jtfStranaA1.getText()), 
												Double.valueOf(jtfStranaB1.getText()), 
												new Uhol(
														Uhol.degreeToRadians(
																Integer.valueOf(jtfUholStupne.getText()), 
																Integer.valueOf(jtfUholMinuty.getText().equals("") ? "0" : jtfUholMinuty.getText()), 
																Double.valueOf(jtfUholSekundy.getText().equals("") ? "0" : jtfUholSekundy.getText())
																)
														)
												);
										
										platno.kresliTrojuholnik(trojuholnik);
										lavyPanel.vypisTrojuholnik(trojuholnik);
										break;
									}
								}
								else
								{
									break;
								}
							}
						break;
					case 2:
							JOptionPane.showMessageDialog(null, "Možnosť ešte nie je implementovaná.", "Oznam", JOptionPane.INFORMATION_MESSAGE);
						break;
					case 3:
						JTextField jtfStranaA2 = new JTextField();
						
						JTextField jtfUhol1Stupne = new JTextField();
						JTextField jtfUhol1Minuty = new JTextField();
						JTextField jtfUhol1Sekundy = new JTextField();
						
						JTextField jtfUhol2Stupne = new JTextField();
						JTextField jtfUhol2Minuty = new JTextField();
						JTextField jtfUhol2Sekundy = new JTextField();
						
						Object[] polozky2 = {
								"Strana: ", jtfStranaA2,
								"1. uhol na strane: ", jtfUhol1Stupne, jtfUhol1Minuty, jtfUhol1Sekundy,
								"2. uhol na strane: ", jtfUhol2Stupne, jtfUhol2Minuty, jtfUhol2Sekundy,
						};
						
						while (jtfStranaA2.getText().equals("") || jtfUhol1Stupne.getText().equals("") || jtfUhol2Stupne.getText().equals(""))
						{
							Integer usu = JOptionPane.showConfirmDialog(null, polozky2, "Zadajte dĺžku stranu a uholy na nej", JOptionPane.OK_CANCEL_OPTION);
						
							if (usu == JOptionPane.OK_OPTION)
							{
								if (jtfStranaA2.getText().equals("") || jtfUhol1Stupne.getText().equals("") || jtfUhol2Stupne.getText().equals(""))
								{
									JOptionPane.showMessageDialog(null, "Je potrebné zadať oba uhly a stranu.", "Chyba", JOptionPane.ERROR_MESSAGE);
								}
								else
								{
									trojuholnik = new Trojuholnik();
									trojuholnik.setUSU(
											Double.valueOf(jtfStranaA2.getText()), 
											new Uhol(
													Uhol.degreeToRadians(
															Integer.valueOf(jtfUhol1Stupne.getText()), 
															Integer.valueOf(jtfUhol1Minuty.getText().equals("") ? "0" : jtfUhol1Minuty.getText()), 
															Double.valueOf(jtfUhol1Sekundy.getText().equals("") ? "0" : jtfUhol1Sekundy.getText())
														)
												), 
											new Uhol(
													Uhol.degreeToRadians(
															Integer.valueOf(jtfUhol2Stupne.getText()), 
															Integer.valueOf(jtfUhol2Minuty.getText().equals("") ? "0" : jtfUhol2Minuty.getText()), 
															Double.valueOf(jtfUhol2Sekundy.getText().equals("") ? "0" : jtfUhol2Sekundy.getText())
														)
												)
										);
									
									platno.kresliTrojuholnik(trojuholnik);
									lavyPanel.vypisTrojuholnik(trojuholnik);
									break;
								}
							}
							else
							{
								break;
							}
						}
						break;
				}
			}
		}
		
	}
}
