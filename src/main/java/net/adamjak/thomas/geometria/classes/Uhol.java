package net.adamjak.thomas.geometria.classes;

public class Uhol
{
	private Double uhol;
	
	public Uhol(Double radians)
	{
		this.uhol = radians;
	}
	
	public static Double degreeToRadians (Integer stupne, Integer minuty, Double sekundy)
	{
		return ((stupne.doubleValue() + minuty.doubleValue() / 60 + sekundy / 3600) * Math.PI) / 180;
	}
	
	public Double getUhol()
	{
		return this.uhol;
	}
	
	public Integer getStupne()
	{
		Double stupne = (this.uhol * 180d ) / Math.PI;
		return stupne.intValue();
	}
	
	public Integer getMinuty()
	{
		Double stupne = (this.uhol * 180d ) / Math.PI;
		stupne = stupne - this.getStupne();
		stupne = stupne * 60d;
		return stupne.intValue();
	}
	
	public Double getSekundy()
	{
		Double stupne = (this.uhol * 180d ) / Math.PI;
		stupne = stupne - this.getStupne();
		stupne = stupne * 60d;
		stupne = stupne - this.getMinuty();
		stupne = stupne * 60d;
		return stupne;
	}
	
	public Double getSinus()
	{
		return Math.sin(this.uhol);
	}
	
	public Double getCosinus()
	{
	 return Math.cos(this.uhol);
	}
	
	public Double getTangens()
	{
		return Math.tan(this.uhol);
	}
	
	public Double getCotangens()
	{
		return 1 / Math.tan(this.uhol);
	}

	public enum UhloveJednotky
	{
		STUPNE, RADIANY
	}
}
