package net.adamjak.thomas.geometria.classes;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;

public class Platno extends JComponent
{
	private static final long serialVersionUID = 1L;
	
	private Trojuholnik trojuholnik;
	private Boolean kreslitTrojuholnik = Boolean.FALSE;
	
	public void kresliTrojuholnik(Trojuholnik t)
	{
		this.trojuholnik = t;
		this.kreslitTrojuholnik = Boolean.TRUE;
		this.repaint();
	}
	
	private void kresliKruznicu(Graphics2D g2, Bod stred, Double polomer)
	{
		Integer x = Double.valueOf(stred.getX() - polomer).intValue();
		Integer y = Double.valueOf(stred.getY() - polomer).intValue();
		Integer size = 2 * polomer.intValue();
		
		g2.drawOval(x, y, size, size);
	}
	
	private void kresliPriemku(Graphics2D g2, Priamka p, Boolean os)
	{
		if (os)
		{
			float[] dash = {2,3,10,3};
			g2.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 1.0f, dash ,2f));
		}
		
		g2.drawLine(
				p.getBodyMimoPlatna(getWidth(), getHeight()).get(0).getX().intValue(), 
				p.getBodyMimoPlatna(getWidth(), getHeight()).get(0).getY().intValue(), 
				p.getBodyMimoPlatna(getWidth(), getHeight()).get(1).getX().intValue(), 
				p.getBodyMimoPlatna(getWidth(), getHeight()).get(1).getY().intValue() 
			);
		
		g2.setStroke(new BasicStroke());
	}
	
	@Override
	public void paintComponent(Graphics g)
	{
		Graphics2D g2 = (Graphics2D) g;
		g2.setBackground(Color.WHITE);
		
		g2.clearRect(0,0,getWidth(),getHeight());
		
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		// Mriezka
		g2.setStroke(new BasicStroke(1));
		g2.setColor(new Color(233, 233, 233));
		for (int i = 50; i < getWidth(); i = i + 50)
		{
			g2.drawLine(0, i, getWidth(), i);
		}
		
		for (int i = 50; i < getWidth(); i = i + 50)
		{
			g2.drawLine(i, 0, i, getHeight());
		}
		
		if (this.kreslitTrojuholnik == Boolean.TRUE) // Kreslenie trojuholnika
		{
			Bod stredPlatna = new Bod(Integer.valueOf(getWidth() / 2).doubleValue(), Integer.valueOf(getHeight() / 2).doubleValue());
			Bod stredTrojuholnika = new Bod(
						(this.trojuholnik.getBodA().getX() + this.trojuholnik.getBodB().getX() + this.trojuholnik.getBodC().getX()) / 3,
						(this.trojuholnik.getBodA().getY() + this.trojuholnik.getBodB().getY() + this.trojuholnik.getBodC().getY()) / 3
					);
			
			Integer posunX = stredPlatna.getX().intValue() - stredTrojuholnika.getX().intValue();
			Integer posunY = stredPlatna.getY().intValue() - stredTrojuholnika.getY().intValue();
			
			Bod a = new Bod(this.trojuholnik.getBodA().getX() + posunX, this.trojuholnik.getBodA().getY() + posunY);
			Bod b = new Bod(this.trojuholnik.getBodB().getX() + posunX, this.trojuholnik.getBodB().getY() + posunY);
			Bod c = new Bod(this.trojuholnik.getBodC().getX() + posunX, this.trojuholnik.getBodC().getY() + posunY);
			
			// Priamky pod stranamy trojuholnika
			g2.setColor(new Color(0x85, 0x85, 0xFF));
			Priamka podA = new Priamka(b,c);
			Priamka podB = new Priamka(a,c);
			Priamka podC = new Priamka(a,b);
			
			this.kresliPriemku(g2, podA, false);
			this.kresliPriemku(g2, podB, false);
			this.kresliPriemku(g2, podC, false);
			
			// Strany trojuholnika
			g2.setColor(Color.BLACK);
			g2.setStroke(new BasicStroke(2));
			
			g2.drawLine(a.getX().intValue(), a.getY().intValue(), b.getX().intValue(), b.getY().intValue()); // usecka AB
			g2.drawLine(b.getX().intValue(), b.getY().intValue(), c.getX().intValue(), c.getY().intValue()); // usecka BC
			g2.drawLine(c.getX().intValue(), c.getY().intValue(), a.getX().intValue(), a.getY().intValue()); // usecka CA
			
			// Bod A
			g2.fillOval(a.getX().intValue() - 5, a.getY().intValue() - 5, 10, 10);
			g2.drawString("A", a.getX().intValue(), a.getY().intValue() + 20);
			
			// Bod B
			g2.fillOval(b.getX().intValue() - 5, b.getY().intValue() - 5, 10, 10);
			g2.drawString("B", b.getX().intValue(), b.getY().intValue() + 20);
			
			// Bod C
			g2.fillOval(c.getX().intValue() - 5, c.getY().intValue() - 5, 10, 10);
			g2.drawString("C", c.getX().intValue(), c.getY().intValue() - 20);
			
			
			// Taznice
			g2.setColor(Color.LIGHT_GRAY);
			g2.setStroke(new BasicStroke(1));
			
			Bod stredStranyA = new Bod(((b.getX() + c.getX())/2),((b.getY() + c.getY())/2));
			Bod stredStranyB = new Bod(((a.getX() + c.getX())/2),((a.getY() + c.getY())/2));
			Bod stredStranyC = new Bod(((b.getX() + a.getX())/2),((b.getY() + a.getY())/2));
			
			Priamka taznicaA = new Priamka(a, stredStranyA);
			Priamka taznicaB = new Priamka(b, stredStranyB);
			Priamka taznicaC = new Priamka(c, stredStranyC);
			
			g2.drawLine(a.getX().intValue(), a.getY().intValue(), stredStranyA.getX().intValue(), stredStranyA.getY().intValue());
			g2.drawLine(b.getX().intValue(), b.getY().intValue(), stredStranyB.getX().intValue(), stredStranyB.getY().intValue());
			g2.drawLine(c.getX().intValue(), c.getY().intValue(), stredStranyC.getX().intValue(), stredStranyC.getY().intValue());
			
			// Vysky
			g2.setColor(Color.ORANGE);
			
			Priamka vyskaNaStranuA = podA.getKolmica(a);
			Priamka vyskaNaStranuB = podB.getKolmica(b);
			Priamka vyskaNaStranuC = podC.getKolmica(c);
			
			this.kresliPriemku(g2, vyskaNaStranuA, false);
			this.kresliPriemku(g2, vyskaNaStranuB, false);
			this.kresliPriemku(g2, vyskaNaStranuC, false);
			
			
			// Osi stran
			g2.setColor(Color.GREEN);
			Priamka osStranyA = podA.getKolmica(stredStranyA);
			Priamka osStranyB = podB.getKolmica(stredStranyB);
			Priamka osStranyC = podC.getKolmica(stredStranyC);
			
			Bod priesecnikOsiAB = osStranyA.priesecnikPriamok(osStranyB);
			Bod priesecnikOsiBC = osStranyB.priesecnikPriamok(osStranyC);
			Bod priesecnikOsiCA = osStranyC.priesecnikPriamok(osStranyA);
			
			// Opisana kruznica 
			Bod stredOpisanejKruznice = new Bod(
					(priesecnikOsiAB.getX() + priesecnikOsiBC.getX() + priesecnikOsiCA.getX()) / 3,
					(priesecnikOsiAB.getY() + priesecnikOsiBC.getY() + priesecnikOsiCA.getY()) / 3
					);
			
			g2.drawOval(stredOpisanejKruznice.getX().intValue() - 5, stredOpisanejKruznice.getY().intValue() - 5, 10, 10);
			
			this.kresliKruznicu(g2, stredOpisanejKruznice, this.trojuholnik.getR());
			
			// Osi uhlov
			g2.setColor(Color.PINK);
			
			Priamka osUhlaCAB1 = podB.getOsiUhla(podC).get(0);
			Priamka osUhlaCAB2 = podB.getOsiUhla(podC).get(1);
			Priamka osUhlaABC1 = podA.getOsiUhla(podC).get(0);
			Priamka osUhlaABC2 = podA.getOsiUhla(podC).get(1);
			Priamka osUhlaBCA1 = podB.getOsiUhla(podA).get(0);
			Priamka osUhlaBCA2 = podB.getOsiUhla(podA).get(1);
			
			this.kresliPriemku(g2, osUhlaCAB1, true);
			this.kresliPriemku(g2, osUhlaCAB2, true);
			
			this.kresliPriemku(g2, osUhlaABC1, true);
			this.kresliPriemku(g2, osUhlaABC2, true);
			
			this.kresliPriemku(g2, osUhlaBCA1, true);
			this.kresliPriemku(g2, osUhlaBCA2, true);
			
			List<Bod> priesecnikyOsiUhlov = new ArrayList<Bod>();
			
			if (!priesecnikyOsiUhlov.contains(osUhlaCAB1.priesecnikPriamok(osUhlaABC1))) priesecnikyOsiUhlov.add(osUhlaCAB1.priesecnikPriamok(osUhlaABC1));
			if (!priesecnikyOsiUhlov.contains(osUhlaCAB1.priesecnikPriamok(osUhlaABC2))) priesecnikyOsiUhlov.add(osUhlaCAB1.priesecnikPriamok(osUhlaABC2));
			
			if (!priesecnikyOsiUhlov.contains(osUhlaCAB1.priesecnikPriamok(osUhlaBCA1))) priesecnikyOsiUhlov.add(osUhlaCAB1.priesecnikPriamok(osUhlaBCA1));
			if (!priesecnikyOsiUhlov.contains(osUhlaCAB1.priesecnikPriamok(osUhlaBCA2))) priesecnikyOsiUhlov.add(osUhlaCAB1.priesecnikPriamok(osUhlaBCA2));
			
			if (!priesecnikyOsiUhlov.contains(osUhlaCAB2.priesecnikPriamok(osUhlaABC1))) priesecnikyOsiUhlov.add(osUhlaCAB2.priesecnikPriamok(osUhlaABC1));
			if (!priesecnikyOsiUhlov.contains(osUhlaCAB2.priesecnikPriamok(osUhlaABC2))) priesecnikyOsiUhlov.add(osUhlaCAB2.priesecnikPriamok(osUhlaABC2));
			
			if (!priesecnikyOsiUhlov.contains(osUhlaCAB2.priesecnikPriamok(osUhlaBCA1))) priesecnikyOsiUhlov.add(osUhlaCAB2.priesecnikPriamok(osUhlaBCA1));
			if (!priesecnikyOsiUhlov.contains(osUhlaCAB2.priesecnikPriamok(osUhlaBCA2))) priesecnikyOsiUhlov.add(osUhlaCAB2.priesecnikPriamok(osUhlaBCA2));
			
			for (Bod bod : priesecnikyOsiUhlov)
			{
				g2.drawOval(bod.getX().intValue() - 5, bod.getY().intValue() - 5, 10, 10);
			}
			
			// pripisane kruznice
			g2.setColor(Color.RED);
			for (Bod bod : priesecnikyOsiUhlov)
			{
				this.kresliKruznicu(g2, bod, bod.getVzdialenost(podA.getKolmica(bod).priesecnikPriamok(podA)));
			}
		}
	}
}
